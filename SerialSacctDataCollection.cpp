/*  Copyright 2019 <COPYRIGHT HOLDER>

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
    associated documentation files (the "Software"),to deal in the Software without restriction, 
    including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
    subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial 
    portions of the Software.
  
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
    INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
    PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
    FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
    ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *
 * Created by Alex Gessinger 06/05/2019
 * The purpose of this program is to write the output of the sacct command to mutipule files
 * sequentially. The MPI version of this program would overflow the Slurm database servers memory.
 * Specifically this example shows pulling one year of data job data from 2018-06-01 to 2019-06-01. 
    Pulling data on a Biweekely basis (every 15 days) to not ovewhelm the SLURM database.
 *
 *
 * The output files will be in the currentDirectory/output/sacct_data_<iteration number>
 *
 * The boolean variable TEST is used to switch from echoing the sacct command and issuing the sacct command.
 * Before running on Cori make sure the TEST variable is set to false.
 *
 * This is mereley an example and if useful to many people I will create a small dynamic application. 
 * 
 */

#include "boost/date_time.hpp"
#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace boost::gregorian;

int main(int argc, char **argv) {
    bool TEST = false;                      /* Used for validation bfore submitting to Cori. */
    const int kQueryWeeks = 2;              /* Duration of weeks to pull at a time. */
    const int kTotalWeeks = 52;             /* Total number of weeks to pull*/
    date start_date {2018,06,01};           /* Initial start date. */
    days days_obj(15);                      /* Needs changed if kQueryWeeks constant is changed. */
    days backtrack(9);                      /* Only used for the last query. Last query shuold only step 6 days not 15 so subtract 9 days. */
    date end_date =  start_date + days_obj; /* Initial end date. */

    /* Date strings used for cmd */
    std::string start;
    std::string end;
    //std::string cmd; /* Holds the string sacct command to be run. */
    char output_file_name[256];
    char cmd[256];

    for(int i = 0; i <= WIY/QUERYWEEKS; i ++){
      /* Reset the C strings. */
      strcpy(cmd,"");

      /* To get exactly a year of data.*/
      if(i == 26)
         end_date = end_date - backtrack;

      /* Give server break/ chance to free memory if we just got data. */
      if(i != 0)
        sleep(5);

      /* Returns YYYY-MM-DD strings we need for sacct command. */
      start = to_iso_extended_string(start_date);
      end   = to_iso_extended_string(end_date);

      /* Put together the command as a C string. C string makes it easer for IPC pipe. */
      if(TEST)
        strcat(cmd,"echo "); /* IF test we echo the sacct command which is then captured and stored in file. */

      strcat(cmd,"sacct --allusers -S ");
      strcat(cmd,start.c_str());
      strcat(cmd," -E ");
      strcat(cmd,end.c_str());
      /* Example of getting specific attributes. */
      //strcat(cmd," --format JobId,JobName,GID,AllocNodes,Elapsed,Timelimit,Partition,Priority,AveCPUFreq,ConsumedEnergy,Status,ExitCode");
      strcat(cmd," --format ALL --delimiter=',' ");

      FILE *fp_pipe;          /* For pipe. */
      FILE* fp_output_file;   /* For sacct cmd output. */
      char buffer[1035];      /* Holds individual lines of sacct cmd output. */

      /* IPC via Pipe to get the output of the sacct command. */
      /* Open the command for reading. */
      fp_pipe = popen(cmd, "r");
      if (fp_pipe == NULL) {
        printf("Iteration %d failed to run the command.\n",i);
        return -1;
      }
      /* Create the unique ouput file name. */
      snprintf(output_file_name,256,"output/sacct_data_%d",i);
      /* Open the output file. */
      fp_output_file = fopen(output_file_name,"a");
      /* Read the output of sacct command and write it to a the output file. */
      while (fgets(buffer, sizeof(buffer)-1, fp_pipe) != NULL) {
        /* Write the data to the file. */
        fprintf(fp_output_file,"%s",buffer);
      }
      
      /* Close the files. */
      fclose(fp_output_file);
      pclose(fp_pipe);

      /* Step to the next period of dates. */
      start_date = end_date;
      end_date = end_date + days_obj;
    }

  return 0;
}
